<?php
class Model_Portfolio extends  Model
{
    public function get_data()
    {
        return array(
            array(
                'Year' => '2018',
                'Site' =>'http://PI.edu.ua',
                'Description'=>'Промо-сайт найкращих розробників
                веб додатків від українського виробника, що випускються у м. Житомир.'
            ),
            array(
                'Year' => '2012',
                'Site' => 'http://PIK.edu.ua',
                'Description' => 'Україномовний сайт випускників скороченого терміну навчання розробників програмного забезпечення.'
            )
        );
    }
}