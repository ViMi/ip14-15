<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Главная</title>

    <link rel="stylesheet" href="../../css/style.css"/>
</head>

<script src="../../js/jsScript.js"></script>

<body>
<?php include 'application/views/'.$content_view; ?>
</body>
</html>
